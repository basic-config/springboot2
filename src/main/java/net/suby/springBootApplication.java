package net.suby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class springBootApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(springBootApplication.class, args);
    }
}
